#Add list of servers to validate to the variable $machines, or use get-content and txt file for list.
#change the services from BITS, BFE to the appropriate list of services.


$machines = @('','') 
#update credential to an account with permissions to validate services
Invoke-Command -ComputerName $machines -ScriptBlock { Get-Service -Name BITS, BFE | select DisplayName, Name, Status, @{Name="LastBootTime";Expression={(Get-CimInstance -ClassName win32_operatingsystem).lastbootuptime}}} -Credential "coxinc\admn_XXXXXXX" | 
Sort-Object PSComputerName | 
Format-Table DisplayName, Name, Status, LastBootTime, PSComputerName -AutoSize #-GroupBy PSComputerName
#3 seconds to complete

Start-Sleep -s 10

#save html file in appropriate location and update path
invoke-item -Path D:\KronosValidation.html